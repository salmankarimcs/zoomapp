/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.innovation.fnfplayer;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.innovation.fnfplayer";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 113;
  public static final String VERSION_NAME = "3.0.7";
}
