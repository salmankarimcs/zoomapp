import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  
  ImageBackground,
  Linking,
  ScrollView,
  Platform,
  Alert,
} from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Loading, TextLink } from "./common";
import { PLATFORM } from "native-base/src/theme/variables/commonColor";
import Constants from "expo-constants";
import deviceStorage from "../services/deviceStorage";
import axios from "axios";
// import https from 'https';
import {
  Item,
  Label,
  Input,
  Button,
  CheckBox,
  Body,
  ListItem,
  Picker,
  Icon,
} from "native-base";
import Modal from "react-native-modal";
import WebView from "react-native-webview";


const io = require("socket.io-client");
const SocketEndpoint = "https://";
class Login extends Component {
 
  constructor(props) {
    super(props);
    this.state = {

      language: 'e',
      languageindex: 0,


      checked: false,
      email: "",
      password: "",
      error: "",
      loading: false,
      visible: false,
      rules: [],

      labels: {
        email: ["Email", "Correo Electrónico"],
        pass: ["Password", "Contrasena"],
        remember: ["Remember Me", "Recuerdame"],
        login: ["Login", "Iniciar Sesion"],
        reghere: ["Don't have an account? Register!", "No tengo una cuenta? Registrarse Aqui"],
        forgot: ["Forgot Password?", "Olvidó Contraseña"],
        schedule: ["Schedule", "Calendario"],
        rules: ["Rules", "Reglas"],
      },
    };
   ;
  }
  async componentDidMount() {
   
    
  }
  

  _renderButton = (text, onPress) => (
    <Button block style={styles.hidebutton} onPress={onPress}>
      <Text style={styles.hidebuttonText}>{text}</Text>
    </Button>
  );
  _renderModalContent = () => {
    const listItems = this.state.rules.map((rule,i) => {
      return (
        <View key={i+"k"} style={{ display: "flex", flexDirection: "row" }}>
          {/* <Text>{'\u2B24'} </Text> */}
          <Text style={styles.rule}>{rule.description}</Text>
        </View>
      );
    });

    return (
      <View style={styles.modalContent}>
        <ScrollView>{listItems}</ScrollView>

        {this._renderButton("Hide", () => {
          this.setState({ visible: false });
        })}
      </View>
    );
  };
  render() {
    const { email, password, error, loading, labels, languageindex } = this.state;
    const { form, section, errorTextStyle, container } = styles;

    return (
      <View style={styles.container}>
        <Modal
          isVisible={this.state.visible}
        //  onBackdropPress={() => this.setState({visible: false})}
        >
          {this._renderModalContent()}
        </Modal>

        <ImageBackground
          resizeMode="cover"
          style={styles.img_back}
          source={require("../../assets/img/login-back.png")}
        >


          <View style={styles.cl20}>

            <Item
              picker
              style={[
                styles.inpt, Platform.OS === "ios" ? styles.pad : null]}
            >
              <Picker
                style={{ color: "white" }}
                mode="dropdown"
                iosIcon={
                  <Icon style={{ color: "white" }} name="arrow-down" />
                }
                textStyle={{ color: "white" }}
                selectedValue={this.state.language}
               
              >
                <Picker.Item label="English" value="e" />
                <Picker.Item label="Spanish" value="s" />
              </Picker>
            </Item>
          </View>

          <View style={styles.logoContainer}>




            <Text style={styles.title}>Brought to you by the makers of</Text>
            <TouchableOpacity
              onPress={() => {
                Linking.openURL("https://www.facebook.com/freenfunbarbingo");
              }}
            >
              <Text style={styles.link}>FNF BarBingo</Text>
              <Text
                style={{
                  fontSize: 15,
                  textAlign: "center",
                  color: "white",
                  opacity: 0.7,
                }}
              >
                {Constants.manifest.version}
                
              </Text>
            </TouchableOpacity>
            <Text style={styles.title}>
              {Platform.OS === "ios" ? "Apple " : "Google "}
              is NOT responsible nor liable in any way for sweepstakes held
              within
            </Text>
          </View>
          <View style={styles.formContainer}>
            {/* <TextInput style={styles.input}
            placeholder="Email"
            placeholderTextColor="rgba(255,255,255,.7)"
            value={email}
            onChangeText={email => this.setState({ email })}
          /> */}
            <Item style={styles.inpt} floatingLabel>
              <Label style={styles.lbl}>{labels.email[languageindex]}</Label>
              <Input
                style={styles.in}
                value={email}
                onChangeText={(email) => this.setState({ email })}
              />
            </Item>

            <Item style={styles.inpt} floatingLabel>
              <Label style={styles.lbl}>{labels.pass[languageindex]}</Label>
              <Input
                style={styles.in}
                value={email}
                onChangeText={(email) => this.setState({ email })}
                secureTextEntry
                value={password}
                onChangeText={(password) => this.setState({ password })}
              />
            </Item>
            <View
              style={{ width: "100%", display: "flex", flexDirection: "row" }}
            >
              <CheckBox
                onPress={() =>
                  this.setState({
                    checked: !this.state.checked,
                  })
                }
                checked={this.state.checked}
                color="#B4148740"
                style={{ padding: 0, marginRight: 20, marginLeft: -8 }}
              />

              <Text style={{ color: "white" }}>{labels.remember[languageindex]}</Text>
            </View>
            {/* <TextInput style={styles.input}
            placeholder="Pasword"
            placeholderTextColor="rgba(255,255,255,.7)"
          secureTextEntry
          value={password}
              onChangeText={password => this.setState({ password })}
          /> */}

            <Text style={errorTextStyle}>{error}</Text>
           
              {/* //  <TouchableOpacity  style={styles.buttonContainer}> */}
              <Button block style={styles.button}>
              {!loading ? (
                <Text style={styles.buttonText}>{labels.login[languageindex]}</Text>
                ) : (




                  //  </TouchableOpacity>
                  <Loading color="#B41487" size={"large"} />
                )}
              </Button>
           
            <TextLink >
              {labels.reghere[languageindex]}
            </TextLink>
            <TextLink >
              {labels.forgot[languageindex]}
            </TextLink>
            <View style={styles.linkcon}>
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL("https://findmebingo.com/game-schedules/");
                }}
              >
                <Text style={styles.link}>{labels.schedule[languageindex]}</Text>
              </TouchableOpacity>
              <Text style={styles.sep}>|</Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ visible: true });
                }}
              >
                <Text style={styles.link}>{labels.rules[languageindex]} </Text>
              </TouchableOpacity>
            </View>







          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = {
  // form: {
  //   width: '100%',
  //   borderTopWidth: 1,
  //   borderColor: '#ddd',
  // },
  // section: {
  //   flexDirection: 'row',
  //   borderBottomWidth: 1,
  //   backgroundColor: '#fff',
  //   borderColor: '#ddd',
  // },
  errorTextStyle: {
    alignSelf: "center",

    fontSize: 18,
    color: "white",
    // marginTop:10,
    marginBottom: 10,
  },
  container: {
    flex: 1,

    // backgroundColor:'#4169e1',
    width: "100%",
    height: "100%",
  },
  logoContainer: {
    // backgroundColor:'red',
    flex: 1,
    alignItems: "center",
    // flexGrow:1,
    justifyContent: "flex-end",
  },
  img_back: {
    flex: 1,
  },
  formContainer: {
    flex: 1,
    alignItems: "center",
    // flexGrow:1,
    justifyContent: "flex-start",
    padding: 20,
  },

  logo: {
    width: 200,
    height: 200,
    // aspectRatio:1
    // aspectRatio: 1
  },
  title: {
    color: "#fff",
    marginTop: 3,
    textAlign: "center",
    // fontSize: 15,
    opacity: 0.9,
  },
  input: {
    height: 40,
    backgroundColor: "rgba(255,255,255,0.2)",
    marginBottom: 20,
    color: "#fff",
    paddingLeft: 10,
  },
  buttonContainer: {
    // backgroundColor:'#2980b9',
    // paddingVertical:15
  },
  buttonText: {
    textAlign: "center",
    color: "#B41487",
    fontWeight: "700",
    fontSize: 20,
  },
  hidebuttonText: {
    textAlign: "center",
    color: "white",
    fontWeight: "700",
    fontSize: 20,
  },
  button: {
    backgroundColor: "white",
  },
  hidebutton: {
    backgroundColor: "#B41487",
  },
  inpt: {
    borderColor: "white",
    marginBottom: 10,
    color: "white",
  },
  lbl: {
    color: "white",
  },
  in: {
    color: "white",
  },
  link: {
    alignSelf: "center",
    color: "white",
    fontSize: 18,
    // fontWeight: '700',
    textDecorationLine: "underline",
  },
  sep: {
    color: "white",
    fontSize: 18,
    paddingLeft: 10,
    paddingRight: 10,
  },
  linkcon: {
    marginTop: 20,
    flex: 1,
    flexDirection: "row",
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  rule: {
    paddingBottom: 10,
  },
  rw: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cl: {
    width: "50%",
  },
  cl20: {
    width: "30%",
    position: 'absolute',
    right: 10,
    top: 40,
    zIndex:40000
    // marginTop:50
  },
  left: {
    paddingRight: 5,
  },
  right: {
    paddingLeft: 5,
  },
  pad: {
    marginTop: 5,
    paddingBottom: 0,
  },
};

export { Login };
