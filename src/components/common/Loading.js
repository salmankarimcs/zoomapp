import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Loading = ({ size ,color}) => {
  return (
    <View style={styles.spinnerContainer}>
      <ActivityIndicator color={color} size={size}/>
    </View>
  );
};

const styles = {
  spinnerContainer: {
    color:'white',
    flex: -1,
    marginTop: 12,
    marginBottom: 12
  }
};
export {Loading}