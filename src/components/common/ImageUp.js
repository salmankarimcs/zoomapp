import React, { useState, useEffect } from "react";
import { Image, View, Platform } from "react-native";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import axios from "axios";
import { ulid } from "ulid";
import { Input, Item, Label, Text, Button } from "native-base";
import { Loading } from "./Loading";
const io = require("socket.io-client");
const SocketEndpoint = "https://";
const socket = io(SocketEndpoint, {
  protocolVersion: 8,

  rejectUnauthorized: false,
});
export default function ImageUp(props) {
  const [image, setImage] = useState(null);
  const [isuploading, setisuploading] = useState(false);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const {
          status,
        } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          // alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    if (isuploading) {
      return;
    }
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      // aspect: [4, 3],
      quality: 1,
    });

    // console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);

      let random = makeid(12);

      let fileExtensionparts = result.uri.split(".");
      let ext = fileExtensionparts[fileExtensionparts.length - 1];
      const data = new FormData();
      data.append("name", random + ext);
      data.append("fileData", {
        uri: result.uri,
        type: "image/" + ext,
        name: random + "." + ext,
      });

      console.log(data);

      setisuploading(true);
      axios({
        url: "https:///api/player/upload",
        method: "POST",
        data: data,
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          //  'Authorization':'Basic YnJva2VyOmJyb2tlcl8xMjM='
        },
      })
        .then(function (response) {
          // console.log("response :", response.data.data.path);
          
          socket.emit("sendstaffimage", {
            name:props.name,
            channel: props.channel,
            url: "https:///server/" + response.data.data.path,
            // time: d.toLocaleDateString() + " " + d.toLocaleTimeString(),
          });

          setisuploading(false);
          setImage(null);
        })
        .catch(function (error) {
          setisuploading(false);
          setImage(null);
          console.log("error from image :");
          console.log(error);
        });
    }
  };
  function makeid(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  return (
    <View
      style={{
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {image && props.preview && (
        <Image
          source={{ uri: image }}
          style={{ width: 200, height: 200, marginBottom: 10 }}
        />
      )}

      <Button style={styles.button} onPress={pickImage}>
        {isuploading ? <Loading color={"white"} /> : null}
        <Text style={styles.buttonText}>
          {isuploading ? "Sending..." : "Send Image"}
        </Text>
      </Button>
    </View>
  );
}

const styles = {
  buttonText: {
    textAlign: "center",
    color: "white",
    fontWeight: "700",
    fontSize: 20,
  },
  buttonTextSec: {
    textAlign: "center",
    color: "#B41487",
    fontWeight: "700",
    fontSize: 20,
  },
  button: {
    // marginTop: 20,
    backgroundColor: "#B41487",
    // borderColorLwhite:'white'
    display: "flex",
    textAlign: "center",
  },
  buttonSec: {
    marginTop: 20,
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "#B41487",

    elevation: 0,
  },
};
