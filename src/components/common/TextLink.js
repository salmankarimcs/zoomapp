import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const TextLink = ({ onPress, children }) => {
  const { button, text } = styles;
  return (
    <View >
      <TouchableOpacity onPress={onPress} style={button}>
        <Text style={text}>
          {children}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  text: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: '700',
    textDecorationLine: 'underline',
    // backgroundColor:'red'
    // paddingTop: 10,
    // paddingBottom: 10,
  
   
  },
  button: {
    marginTop: 10,
    // marginBottom: 5,
   
  }
};

export { TextLink };