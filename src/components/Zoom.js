import React, { useEffect } from 'react'
import { StyleSheet, Text, View , Pressable, TouchableOpacity} from 'react-native'
import ZoomUs from 'react-native-zoom-us';
const Zoom = () => {
    useEffect(()=>{
        startZoom()
      },[])
      // initialize with extra config
      const startZoom = async () => {
        await ZoomUs.initialize({
          clientKey: "i1QmXoMmIHrj9DAlGMwySBlJOpWerakpqECC",
          clientSecret: "dDYy4tMyPlVSeFaHTYL55SHBIrutYLRREEAP",
          domain: "zoom.us",
        });
      };
      
      // Join Meeting
      const joinMeeting = async () => {
        await ZoomUs.joinMeeting({
          userName: "Arif Malik",
          meetingNumber: "81965593377", // meetingID
          password: "029821", // Passcode
          zoomAccessToken:
            "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImVzZHhNTFVzVEpTcTRBMjljMVV2ZFEiLCJleHAiOjE2NDIxMDIxMTMsImlhdCI6MTY0MTQ5NzMxNH0.prFpEjcaAnxI8izjxAAxDcGr9e7KjNErapSBk0SL9eM",
        });
      };
    return (
        <View style={styles.main}>
                <TouchableOpacity onPress={()=> joinMeeting()} style={styles.btn}>
                <Text style={styles.btnText}> Join Meeting</Text>
                </TouchableOpacity>
       
        </View>
    )
}

export default Zoom

const styles = StyleSheet.create({
    main:{ 
        flex:1, 
        alignItems:'center',
        justifyContent:'center',
        width:'100%'
    },
    btn:{ width:'90%', 
    height:50,
     alignSelf:'center', 
     justifyContent:'center',
     alignItems:'center', 
     backgroundColor:'green',
    
    },
    btnText:{color:'#ffffff'}
})
