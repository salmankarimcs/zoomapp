
import AsyncStorage from '@react-native-async-storage/async-storage';
const deviceStorage = {
  async saveKey(key, value) {

    try {
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },
  async loadJWT() {
    try {
      const value = await AsyncStorage.getItem("id_token");
      if (value !== null) {
        this.setState({
          jwt: value,
          loading: false,
        });
      } else {
        this.setState({
          loading: false,
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },
  async loadMC() {
    try {
      const value = await AsyncStorage.getItem("mc");
      if (value !== null) {
        this.setState({
          hexColor: value,
         
        });
      } else {
        this.setState({
          hexColor: "#B41487",
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },

  async loadUn() {
    try {
      const value = await AsyncStorage.getItem("un");
      if (value !== null) {
        this.setState({
          email: value,
         
        });
      } else {
        this.setState({
          email: "",
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },

  async loadPw() {
    try {
      const value = await AsyncStorage.getItem("pw");
      if (value !== null) {
        this.setState({
          password: value,
         
        });
      } else {
        this.setState({
          password: "",
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },

  async loadAnswer() {
    try {
      const value = await AsyncStorage.getItem("answer");
      if (value !== null) {
       
        this.setState({
          answer: value,
         
        });
      } else {
        this.setState({
          answer: "",
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },
  async loadMute() {
    try {
      const value = await AsyncStorage.getItem("mute");
      if (value !== null) {
        // alert(value)
        if(value=='1'){
          this.setState({
            mute: true,
           
          });
        }
        else{
          this.setState({
            mute: false,
           
          });
        }
        
      } else {
        this.setState({
          mute: false,
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },

  async loadMute2() {
    try {
      const value = await AsyncStorage.getItem("mute2");
      if (value !== null) {
        // alert(value)
        if(value=='1'){
          this.setState({
            mute2: true,
           
          });
        }
        else{
          this.setState({
            mute2: false,
           
          });
        }
        
      } else {
        this.setState({
          mute2: true,
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },
  async loadMute3() {
    try {
      const value = await AsyncStorage.getItem("mute3");
      if (value !== null) {
        // alert(value)
        if(value=='1'){
          this.setState({
            mute3: true,
           
          });
        }
        else{
          this.setState({
            mute3: false,
           
          });
        }
        
      } else {
        this.setState({
          mute3: true,
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },

  async loadMute4() {
    try {
      const value = await AsyncStorage.getItem("mute4");
      if (value !== null) {
        // alert(value)
        if(value=='1'){
          this.setState({
            mute4: true,
           
          });
        }
        else{
          this.setState({
            mute4: false,
           
          });
        }
        
      } else {
        this.setState({
          mute4: true,
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },
  async loadMute5() {
    try {
      const value = await AsyncStorage.getItem("mute5");
      if (value !== null) {
        // alert(value)
        if(value=='1'){
          this.setState({
            mute5: true,
           
          });
        }
        else{
          this.setState({
            mute5: false,
           
          });
        }
        
      } else {
        this.setState({
          mute5: true,
        });
      }
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },
  async loadRMute() {
    try {
      const value = await AsyncStorage.getItem("radiomute");
      if (value !== null) {
        // alert(value)
        if(value=='1'){
          this.setState({
            radiomute: true,
           
          });
        }
        else{
          this.setState({
            radiomute: false,
           
          });
        }
        
      } else {
        this.setState({
          radiomute: true,
        });
      }

    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  },

  

  async deleteJWT() {
    try{
      await AsyncStorage.removeItem('id_token')
      .then(
        () => {
          this.setState({
            jwt: ''
          })
        }
      );
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message);
    }
  },
  async loadUser() {
    let value=null
    try {
       value = await AsyncStorage.getItem("user");
        // console.log(JSON.parse(value))
        return JSON.parse(value);
     
    } catch (error) {
      console.log("AsyncStorage Error: " + error.message);
    }
  
   },

};

export default deviceStorage;
